/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wiker.codescounter.constant;

/**
 *
 * @author wiker
 */
public class Constants {
    
    public final static String PROJECT_PATH_TEXT = "请选择项目路径";
    
    public final static String MESSAGE_NO_DIRECTORY = "请选择一个文件夹！";
    public final static String MESSAGE_NO_EXIST_DIRECTORY = "目录不存在！";
    
    public final static String[] EXCLUSIVE_FILE = {".class",".project",".classpath",".jar",".prefs",".component"}; 
}
