/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wiker.codescounter.bean;

/**
 *
 * @author wiker
 */
public class CodeBean implements java.io.Serializable {
    
    private String path;                //路径
    private String type;                //文件类型
    private String extName;             //扩展名，如java
    private Long totalLineNumbers;      //代码总行数
    private Long codeLineNumbers;       //代码行数
    private Long commentLineNumbers;    //注释行数
    private Long blankLineNumbers;      //空行数
    private String fileLength;          //文件大小

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the extName
     */
    public String getExtName() {
        return extName;
    }

    /**
     * @param extName the extName to set
     */
    public void setExtName(String extName) {
        this.extName = extName;
    }

    /**
     * @return the totalLineNumbers
     */
    public Long getTotalLineNumbers() {
        return totalLineNumbers;
    }

    /**
     * @param totalLineNumbers the totalLineNumbers to set
     */
    public void setTotalLineNumbers(Long totalLineNumbers) {
        this.totalLineNumbers = totalLineNumbers;
    }

    /**
     * @return the codeLineNumbers
     */
    public Long getCodeLineNumbers() {
        return codeLineNumbers;
    }

    /**
     * @param codeLineNumbers the codeLineNumbers to set
     */
    public void setCodeLineNumbers(Long codeLineNumbers) {
        this.codeLineNumbers = codeLineNumbers;
    }

    /**
     * @return the commentLineNumbers
     */
    public Long getCommentLineNumbers() {
        return commentLineNumbers;
    }

    /**
     * @param commentLineNumbers the commentLineNumbers to set
     */
    public void setCommentLineNumbers(Long commentLineNumbers) {
        this.commentLineNumbers = commentLineNumbers;
    }

    /**
     * @return the blankLineNumbers
     */
    public Long getBlankLineNumbers() {
        return blankLineNumbers;
    }

    /**
     * @param blankLineNumbers the blankLineNumbers to set
     */
    public void setBlankLineNumbers(Long blankLineNumbers) {
        this.blankLineNumbers = blankLineNumbers;
    }

    /**
     * @return the fileLength
     */
    public String getFileLength() {
        return fileLength;
    }

    /**
     * @param fileLength the fileLength to set
     */
    public void setFileLength(String fileLength) {
        this.fileLength = fileLength;
    }
}
