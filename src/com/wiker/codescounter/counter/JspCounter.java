/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wiker.codescounter.counter;

import com.wiker.codescounter.bean.CodeBean;
import com.wiker.codescounter.util.FileUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author wiker
 */
public class JspCounter {
    
    /**
     * jsp文件统计工具
     * @param file
     * @return 
     */
    public static CodeBean getByFileForJsp(File file) {
        BufferedReader reader = null;
        CodeBean bean = new CodeBean();

        String type;
        long totalLineNumbers = 0;
        long codeLineNumbers = 0;
        long commentLineNumbers = 0;
        long blankLineNumbers = 0;

        bean.setPath(file.getPath());
        bean.setExtName(file.getPath().substring(file.getPath().lastIndexOf(".") + 1, file.getPath().length()));
        bean.setFileLength(FileUtils.getFormatSize(file.length()));

        try {
            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            boolean isComment = false;
            boolean isComment1 = false;
            // 一次读入一行，直到读入null为文件结束
            while ((tempString = reader.readLine()) != null) {
                totalLineNumbers++;

                //统计空行
                if (tempString == null || tempString.trim().equals("")) {
                    blankLineNumbers++;
                    continue;
                }

                if (tempString.trim().endsWith("-->")) {
                    isComment = false;
                    commentLineNumbers++;
                    continue;
                }

                if (isComment) {
                    commentLineNumbers++;
                    continue;
                }
                
                if (tempString.trim().endsWith("--%>")) {
                    isComment1 = false;
                    commentLineNumbers++;
                    continue;
                }

                if (isComment1) {
                    commentLineNumbers++;
                    continue;
                }

                //类和方法前注释
                if (tempString.trim().startsWith("<!--") && tempString.trim().endsWith("-->")) {
                    commentLineNumbers++;
                    continue;
                }
                
                //类和方法前注释
                if (tempString.trim().startsWith("<!--")) {
                    commentLineNumbers++;
                    isComment = true;
                    continue;
                }
                
                //JSP注释
                if (tempString.trim().startsWith("<%--") && tempString.trim().endsWith("--%>")) {
                    commentLineNumbers++;
                    continue;
                }
                
                //JSP注释
                if (tempString.trim().startsWith("<%--")) {
                    commentLineNumbers++;
                    isComment = true;
                    continue;
                }

                codeLineNumbers++;
            }

            bean.setTotalLineNumbers(totalLineNumbers);
            bean.setBlankLineNumbers(blankLineNumbers);
            bean.setCodeLineNumbers(codeLineNumbers);
            bean.setCommentLineNumbers(commentLineNumbers);

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
        return bean;
    }
}
