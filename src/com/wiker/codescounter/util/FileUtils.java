/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wiker.codescounter.util;

import com.wiker.codescounter.constant.Constants;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author wiker
 */
public class FileUtils {

    List<File> fileList = new ArrayList<File>();

    /**
     * 获取目录下所有文件
     *
     * @param path
     * @return
     */
    public List<File> getAllFilesByDirectory(String path) {
        File rootFile = new File(path);
        File[] files = rootFile.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                if(!file.getPath().endsWith(".settins")){
                    getAllFilesByDirectory(file.getPath());
                }
            } else {
                fileList.add(file);
            }
        }
        return fileList;
    }

    /**
     * 获取目录下的所有文件，并过滤掉不需要的文件
     *
     * @param path
     * @return
     */
    public List<File> getFilesExclusive(String path) {
        List<File> fileList = getAllFilesByDirectory(path);
        List<File> reFileList = new ArrayList<File>();
        for (File file : fileList) {
            String fileExtName = file.getPath().toLowerCase();
            boolean isContinue = false;
            for (String extName : Constants.EXCLUSIVE_FILE) {
                if (fileExtName.lastIndexOf(extName) > 0) {
                    isContinue = true;
                    break;
                }
            }
            if (!isContinue) {
                reFileList.add(file);
            }
        }
        return reFileList;
    }
    
    /**
     * 获取文件大小
     * @param size
     * @return 
     */
    public static String getFormatSize(double size) {
        double kiloByte = size / 1024;
        if (kiloByte < 1) {
            return size + "Byte(s)";
        }

        double megaByte = kiloByte / 1024;
        if (megaByte < 1) {
            BigDecimal result1 = new BigDecimal(Double.toString(kiloByte));
            return result1.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " KB";
        }

        double gigaByte = megaByte / 1024;
        if (gigaByte < 1) {
            BigDecimal result2 = new BigDecimal(Double.toString(megaByte));
            return result2.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " MB";
        }

        double teraBytes = gigaByte / 1024;
        if (teraBytes < 1) {
            BigDecimal result3 = new BigDecimal(Double.toString(gigaByte));
            return result3.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " GB";
        }
        BigDecimal result4 = new BigDecimal(teraBytes);
        return result4.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " TB";
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        FileUtils f = new FileUtils();
        List<File> allFiles = f.getFilesExclusive("/home/wiker/Desktop/HR");
        for (File file : allFiles) {
            System.out.println(file.getPath());
        }
        System.out.println(allFiles.size());
    }
}
