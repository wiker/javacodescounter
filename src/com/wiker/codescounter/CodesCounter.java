/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wiker.codescounter;

import java.awt.Toolkit;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author wiker
 */
public class CodesCounter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String laf = UIManager.getSystemLookAndFeelClassName();
        try {
            // System.out.println("========================"+laf);   
            UIManager.setLookAndFeel(laf);
            MainJFrame main = new MainJFrame();
            //居中显示
            double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
            double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
            main.setLocation( (int) (width - main.getWidth()) / 2,(int) (height - main.getHeight()) / 2);
            //显示当前窗体
            main.setVisible(true);
        } catch (UnsupportedLookAndFeelException exc) {
            System.err.println("Warning: UnsupportedLookAndFeel: " + laf);
        } catch (Exception exc) {
            System.err.println("Error loading " + laf + ": " + exc);
        }
    }
}
